package th.co.azay.leadservice.createleads;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import th.co.azay.leadservice.api.LeadsApi;
import th.co.azay.quote.createquotes.CreateQuotesService;


    //mvn spring-boot:run -Dspring-boot.run.profiles=foo,bar


@Component
public class CreateLeadsController implements LeadsApi {

    @Autowired
    CreateQuotesService createQuotesService;

    @Override
    public ResponseEntity<String> createQuote(String xApplicationName, String quote) {
        return null;
    }

    @Override
    public ResponseEntity<String> getQuote(String authorization, String date, String xRequestID, String xApplicationName, String quoteId) {
        return null;
    }

    @Override
    public ResponseEntity<String> updateQuote(String authorization, String date, String xRequestID, String xApplicationName, String quote) {
        return null;
    }
}
