package th.co.azay.leadservice.api;


import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//
// GENERATED CLASS, DO NOT EDIT
//

@Api(value = "/v1/api/quotes")
@RequestMapping(value="/v1/api")
public interface  LeadsApi {

    @RequestMapping(value="/quotes",method=RequestMethod.POST,consumes={ "application/json" } , produces={ "application/json" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden Access"),
            @ApiResponse(code = 404, message = "Resource Not Found"),
            @ApiResponse(code = 500, message = "Internal server error occurred"),
            @ApiResponse(code = 503, message = "Service Unavailable"),
            @ApiResponse(code = 504, message = "Gateway Timeout")
    })
    ResponseEntity<String> createQuote(
            //@ApiParam(value = "Bearer oauth2_access_token", required = true)
            //@RequestHeader("Authorization") String authorization,
            //@ApiParam(value = "RFC 7231 Date/Time Formats - https://tools.ietf.org/html/rfc7231#section-7.1.1.1 ,e.g : Sun, 06 Nov 1994 08:49:37 GMT", required = true)
            //@RequestHeader("Date") String date,
            //@ApiParam(value = "Unique request ID provided by the partner for logging and tracing transactions.", required = true)
            //@RequestHeader("X-Request-ID") String xRequestID,
            //@ApiParam(value = "Free text application name", required = true)
            @RequestHeader("X-Application-Name") String xApplicationName,
            @ApiParam(value = "", required = true)
            @RequestBody String quote);

    @RequestMapping(value="/quotes/{quoteId}",method=RequestMethod.GET , produces={ "application/json" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden Access"),
            @ApiResponse(code = 404, message = "Resource Not Found"),
            @ApiResponse(code = 500, message = "Internal server error occurred"),
            @ApiResponse(code = 503, message = "Service Unavailable"),
            @ApiResponse(code = 504, message = "Gateway Timeout")
    })
    ResponseEntity<String> getQuote(@ApiParam(value = "Bearer oauth2_access_token", required = true)
                                               @RequestHeader("Authorization")
                                                       String authorization,
                                               @ApiParam(value = "RFC 7231 Date/Time Formats - https://tools.ietf.org/html/rfc7231#section-7.1.1.1 ,e.g : Sun, 06 Nov 1994 08:49:37 GMT", required = true)
                                               @RequestHeader("Date")
                                                       String date,
                                               @ApiParam(value = "Unique request ID provided by the partner for logging and tracing transactions.", required = true)
                                               @RequestHeader("X-Request-ID")
                                                       String xRequestID,
                                               @ApiParam(value = "Free text application name", required = true)
                                               @RequestHeader("X-Application-Name")
                                                       String xApplicationName,
                                               @ApiParam(value = "", required = true)
                                               @PathVariable("quoteId")
                                                       String quoteId);

    @RequestMapping(value="/quotes",method=RequestMethod.PUT,consumes={ "application/json" } , produces={ "application/json" })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden Access"),
            @ApiResponse(code = 404, message = "Resource Not Found"),
            @ApiResponse(code = 500, message = "Internal server error occurred"),
            @ApiResponse(code = 503, message = "Service Unavailable"),
            @ApiResponse(code = 504, message = "Gateway Timeout")
    })
    ResponseEntity<String> updateQuote(@ApiParam(value = "Bearer oauth2_access_token", required = true)
                                                  @RequestHeader("Authorization")
                                                          String authorization,
                                                  @ApiParam(value = "RFC 7231 Date/Time Formats - https://tools.ietf.org/html/rfc7231#section-7.1.1.1 ,e.g : Sun, 06 Nov 1994 08:49:37 GMT", required = true)
                                                  @RequestHeader("Date")
                                                          String date,
                                                  @ApiParam(value = "Unique request ID provided by the partner for logging and tracing transactions.", required = true)
                                                  @RequestHeader("X-Request-ID")
                                                          String xRequestID,
                                                  @ApiParam(value = "Free text application name", required = true)
                                                  @RequestHeader("X-Application-Name")
                                                          String xApplicationName,
                                                  @ApiParam(value = "", required = true)
                                                  @RequestBody
                                                          String quote);

}
